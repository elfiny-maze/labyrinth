extends Label


const GOOD_HEALTH := [Color("33ff00"), 75]
const AVERAGE_HEALTH := [Color("f0fc25"), 40]
const BAD_HEALTH := Color("fc5725")


func _ready() -> void:
    text = "Health: 100"
    set("custom_colors/font_color", GOOD_HEALTH[0])


func _on_Health_changed(new_health: int) -> void:
    text = "Health: %d" % new_health
    if new_health >= GOOD_HEALTH[1]:
        set("custom_colors/font_color", GOOD_HEALTH[0])
    elif new_health >= AVERAGE_HEALTH[1]:
        set("custom_colors/font_color", AVERAGE_HEALTH[0])
    else:
        set("custom_colors/font_color", BAD_HEALTH)
