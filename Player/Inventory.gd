extends PopupPanel
class_name Inventory


signal player_pause(is_paused)

func _ready() -> void:
    set_process(true)
    if self.connect("player_pause", $"/root/Main/Player", "custom_pause") != OK:
        Logging.error("Failed to connect Invenory to Player.")
    
    var screen := ($"/root/" as Viewport).size
    margin_left = screen.x / 6
    margin_top = screen.y / 8
    margin_right = -margin_left
    margin_bottom = -margin_top


func _on_Inventory_about_to_show():
    emit_signal("player_pause", true)


func _on_Inventory_popup_hide():
    emit_signal("player_pause", false)
