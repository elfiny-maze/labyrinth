extends KinematicBody
class_name PlayerController

##############################
# PLAYER STATS
##############################

const MAX_HEALTH: int = 100
var health: int = MAX_HEALTH

onready var Hand : RayCast = $Head/Camera/Hand

##############################
# CAMERA BEHAVIOR
##############################

# current mouse sensitivity; written as a constant
onready var MOUSE_SENSITIVITY: float =\
        deg2rad(Controls.OTHER_VALUES["mouse_sensitivity"])

# max and min vertical angles for camera
const MOUSE_MAX_ANGLE: float = 80.0 * PI / 180.0
const MOUSE_MIN_ANGLE: float = -80.0 * PI / 180.0

# How fast we move.
var velocity := Vector3.ZERO
# Walk direction of movement.
var walk_direction := Vector2.ZERO

##############################

# Flying or walking?
var is_flying: bool = false
var is_interacting: bool = false

const ACTION_DIRECTION := [
    ["move_forward",   Vector3( 0,  0, -1)],
    ["move_backwards", Vector3( 0,  0,  1)],
    ["move_left",      Vector3(-1,  0,  0)],
    ["move_right",     Vector3( 1,  0,  0)],
]

##############################
# FLY CONSTANTS
##############################

# Max speed of horizontal movement.
const FLY_SPEED: float = 15.0
# How quick we gain current speed.
const FLY_ACCEL: float = 10.0

##############################
# WALK CONSTANTS
##############################

const GRAVITY: float = 9.8 * 3
const MAX_SPEED: float = 8.0
const MAX_SPRINT: float = 20.0
const ACCEL: float = 3.0
const DEACCEL: float = 12.0

# Jumping.
const JUMP_VELOCITY: float = 9.8

##############################
# PLAYER INTERACTION
##############################

signal interaction(next_level)
signal died
signal health_changed(new_value)


func _ready() -> void:
    Controls.set_player(self)


func _input(event: InputEvent) -> void:
    # To work with mouse movements.
    if event is InputEventMouseMotion:
        aim(event.relative)
    # Flashlight off\on.
    if Input.is_action_just_pressed("flashlight"):
        ($Head/Camera/FlashLight as OmniLight).visible =\
                !($Head/Camera/FlashLight as OmniLight).visible
    if Input.is_action_pressed("player_interaction"):
        if not is_interacting:
            is_interacting = true
            interact()
    else:
        is_interacting = false


func _physics_process(delta: float) -> void:
    if not is_flying: walk(delta)
    else: fly(delta)


# Calculate unnormalized movement direction by camera view and input data.
func calc_view_direction() -> Vector3:
    # Direction of movement relatively default basis.
    var direction = Vector3.ZERO

    # Apply movement direction by input signals.
    for i in ACTION_DIRECTION:
        if Input.is_action_pressed(i[0]):
            direction += i[1]

    # Return direction relatively camera view direction
    return ($Head as Spatial).get_global_transform().basis * direction


# Update walk direction by movement direction.
func calc_walk_direction() -> Vector2:
    var direction: Vector3 = calc_view_direction()
    return Vector2(direction.x, direction.z).normalized()


func walk(delta: float) -> void:
    # Create horizontal velocity.
    var horiz_velo = Vector2(velocity.x, velocity.z)

    # Select right speed.
    var speed: float = MAX_SPEED if not Input.is_action_pressed("move_sprint")\
            else MAX_SPRINT

    # Apply gravity.
    velocity.y -= GRAVITY * delta

    if is_on_floor():
        # If jumping then jump else stop falling.
        velocity.y = JUMP_VELOCITY if Input.is_action_pressed("jump") else 0.0

        # Update walk direction
        walk_direction = calc_walk_direction()

    # Where would we go on the max speed.
    var target: Vector2 = walk_direction * speed

    # We are moving or slowing down, so choose corrent acceleration.
    var acceleration: float = ACCEL if target.length_squared() > 0 else DEACCEL

    # Calculate the portion of distance to go.
    horiz_velo = horiz_velo.linear_interpolate(target, acceleration * delta)

    # Convert horizontal velocity to real without loosing vertical velocity.
    velocity = Vector3(horiz_velo.x, velocity.y, horiz_velo.y)

    # If we are really need to move then move.
    if velocity.length_squared() > 0:
        velocity = move_and_slide(velocity, Vector3(0, 1, 0))


func fly(delta: float) -> void:
    # Checking input and changing directions.
    var fly_direction: Vector3 = calc_view_direction().normalized()

    # Where would we go on the max speed.
    var target: Vector3 = fly_direction * FLY_SPEED

    # Calculate the portion of distance to go.
    velocity = velocity.linear_interpolate(target, FLY_ACCEL * delta)

    if velocity.length_squared() > 0:
        velocity = move_and_slide(velocity)


func aim(camera_change: Vector2) -> void:
    if camera_change.length_squared() > 0:
        var rotation = Vector3(camera_change.y, camera_change.x, 0)
        rotate_player(-MOUSE_SENSITIVITY * rotation)


func rotate_player(angles: Vector3, inertion: bool = false) -> void:
    var rotation: Vector3 = $Head.transform.basis.get_euler() + angles
    rotation.x = max(MOUSE_MIN_ANGLE, rotation.x)
    rotation.x = min(MOUSE_MAX_ANGLE, rotation.x)
    ($Head as Spatial).transform.basis = Basis(rotation)
    if inertion:
        velocity = Basis(angles) * velocity
        walk_direction = calc_walk_direction()


func interact() -> void:
    Logging.debug_message("Trying to interact")
    if Hand.is_colliding():
        var door_name = Hand.get_collider().name
        Logging.debug_message("Interacting with %s" % door_name)
        emit_signal("interaction", door_name)


func toggle_fly() -> void:
    is_flying = not is_flying


func damage(damage_value: int) -> void:
    health -= damage_value
    emit_signal("health_changed", health if health > 0 else 0)
    if health <= 0:
        emit_signal("died")


func heal(restored_value: int) -> void:
    if health + restored_value >= MAX_HEALTH:
        health = MAX_HEALTH
    else:
        health += restored_value
    emit_signal("health_changed", health)


func custom_pause(is_paused: bool) -> void:
    var is_running := not is_paused
    set_process(is_running)
    set_process_input(is_running)
    set_physics_process(is_running)
    set_process_unhandled_input(is_running)
    set_process_unhandled_key_input(is_running)
    if is_paused:
        Input.set_mouse_mode(Input.MOUSE_MODE_CONFINED)
    else:
        Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
