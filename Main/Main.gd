extends Spatial

const SCREENSHOT_PATH := "user://screenshots/"
const START_LEVEL := "lab_east01"

const ALL_LEVELS := {
    "Level01" : "res://Levels/Level/TestLevel.tscn",
    "Level02" : "res://Levels/Level/TestLevel2.tscn",
    "Outside" : "res://Levels/Level/TestOutside.tscn",
    "lab_east01" : "res://Levels/Level/lab_east01.tscn",
}

const DOORS := {
    "Door1": ["Level01", "Level02"],
    "Door2": ["Level02", "Outside"],
}

onready var FPS_display := $GUI/FPS

var current_level: String = START_LEVEL
var AVAILABLE_LEVELS: Dictionary = {}


func _ready() -> void:
    # Loading start level.
    if not level_translation(START_LEVEL):
        print("Failed to load start level")
        get_tree().quit(-1)

    # Capture mouse.
    Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

    # Connecting player.
    if $Player.connect("interaction", self, "_on_Player_interaction") != OK:
        print("Failed to connect Player to Main")
        get_tree().quit(-1)

    if $Player.connect("died", self, "_on_Player_death") != OK:
        print("Failed to connect Player to Main")
        get_tree().quit(-1)


func _process(_delta: float) -> void:
    if FPS_display.visible:
        FPS_display.text = "%d FPS" %\
         Engine.get_frames_per_second()


func _input(_event: InputEvent) -> void:
    if Input.is_action_just_pressed("take_screenshot"):
        take_screenshot()
    if Input.is_action_just_pressed("hurt"):
        $Player.damage(5)
    if Input.is_action_just_pressed("heal"):
        $Player.heal(2)


func take_screenshot() -> void:
    get_viewport().set_clear_mode(Viewport.CLEAR_MODE_ONLY_NEXT_FRAME)
    var dir := Directory.new()
    if dir.open(SCREENSHOT_PATH) != OK:
        if dir.make_dir(SCREENSHOT_PATH) != OK:
            Logging.error("Failed to save a screenshot: couldn't open a folder")
            return

    var cur_date := OS.get_datetime()
    var screenshot_name := \
            "%s/%s/screenshot_%d%02d%02d%02d%02d%02d.png" %\
            [OS.get_user_data_dir(), "screenshots",
            cur_date.year, cur_date.month, cur_date.day,
            cur_date.hour, cur_date.minute, cur_date.second]
    if OS.get_name() == "Windows":
        screenshot_name = screenshot_name.replace("/", "\\")
    else:
        screenshot_name = screenshot_name.replace("\\", "/")

    yield(get_tree(), "idle_frame")
    yield(get_tree(), "idle_frame")
    var capture: Image = get_viewport().get_texture().get_data()
    capture.flip_y()
    if capture.save_png(screenshot_name) == OK:
        var message := "Screenshot saved as \"%s\"." % screenshot_name
        Logging.message(message)
    else:
        Logging.error("Failed to save a screenshot as a file.")


func level_translation(to_scene: String, door_name: String = "") -> bool:
    var spawn_path: String
    var node: Node
    var rotation := Vector3.ZERO

    if not AVAILABLE_LEVELS.has(to_scene):
        var new_level := load(ALL_LEVELS[to_scene])
        if new_level == null:
            Logging.error("Failed to load a next scene")
            return false
        AVAILABLE_LEVELS[to_scene] = new_level.instance()

    if door_name == "":
        spawn_path = "Level/SpawnPoints/Start"
    else:
        spawn_path = "Level/Doors/%s" % door_name
        node = get_node(spawn_path)
        if not node:
            return false
        rotation = node.transform.basis.get_euler()
        remove_child($Level)

    var level: Node = AVAILABLE_LEVELS[to_scene]
    level.name = "Level"
    level.add_to_group("pausable")
    add_child(level)
    current_level = to_scene
    node = get_node(spawn_path)
    if not node:
        return false
    var origin: Vector3 = node.transform.origin
    origin += node.transform.basis * Vector3(0, -0.5, 1)
    $Player.transform.origin = origin
    $Player.rotate_player(rotation - node.transform.basis.get_euler(), true)
    return true


func _on_Player_interaction(door_name: String) -> void:
    var next_level: String = DOORS[door_name][0]
    if next_level == current_level:
        next_level = DOORS[door_name][1]
    Logging.debug_message("From %s to %s" % [current_level, next_level])
    if not level_translation(next_level, door_name):
        Logging.error("Failed to translate to %s" % next_level)


func _on_Player_death() -> void:
    Logging.debug_success("You died.")
    if get_tree().reload_current_scene() != OK:
        Logging.special("Your death killed the game.")
        get_tree().quit(-1)
