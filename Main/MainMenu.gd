extends Node


export var show_main_menu := true


func _ready():
    Config.load_from_config_file()
    if not show_main_menu:
        _on_NewGame_pressed()


func _load_game():
    var packed_scene := load("Main/MainTest.tscn")
    var game : Node = packed_scene.instance()
    get_tree().get_root().call_deferred("add_child", game)

# Starting a new game.
func _on_NewGame_pressed():
    _load_game()
    queue_free()

# Exiting.
func _on_Exit_pressed():
    get_tree().quit(0)
