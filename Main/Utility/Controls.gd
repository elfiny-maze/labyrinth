extends Node


const _CONTROLS_PATH := "user://controls.ini"
const _PASS := "whhufwhwefhwejhwefjhwehkfwehklfe"
const DEFAULT_BINDINGS := {
    "move_forward" : [KEY_W, KEY_UP],
    "move_backwards" : [KEY_S, KEY_DOWN],
    "move_right" : [KEY_D, KEY_RIGHT],
    "move_left" : [KEY_A, KEY_LEFT],
    "jump" : [KEY_SPACE, KEY_UNKNOWN],
    "move_sprint" : [KEY_SHIFT, KEY_UNKNOWN],
    "flashlight" : [KEY_F, KEY_UNKNOWN],
    "player_interaction" : [KEY_E, KEY_UNKNOWN],
    "take_screenshot" : [KEY_F2, KEY_UNKNOWN],
    "dev_console" : [KEY_QUOTELEFT, KEY_UNKNOWN],
    "inventory" : [KEY_TAB, KEY_UNKNOWN],
   }

const OTHER_VALUES := {
    "mouse_sensitivity" : 0.8,
}


onready var _control_value := DEFAULT_BINDINGS.hash() * OTHER_VALUES.hash()
onready var ControlsMenu : VBoxContainer = null
onready var Player : PlayerController = null


func _ready() -> void:
    var controls := ConfigFile.new()
    var file := File.new()
    
    if not file.file_exists(_CONTROLS_PATH):
        # Creating controls.ini.
        if file.open_encrypted_with_pass(_CONTROLS_PATH, File.WRITE, _PASS) !=\
                OK:
            print("Failed to create controls file.")
            get_tree().quit(-1)
        file.close()
    
    if controls.load_encrypted_pass(_CONTROLS_PATH, _PASS) != OK:
        # File can exit but it be corrupted?
        print("Failed to load user://controls.ini.")
        set_default()
    else:
        if controls.get_value("general", "version", 0) != _control_value:
            # Different version.
            print("Controls file is outdated.")
            clear_bindings()
            set_default()

# Setting default keybindings.
func set_default() -> void:
    var controls := ConfigFile.new()
    
    if controls.load_encrypted_pass(_CONTROLS_PATH, _PASS) != OK:
        Logging.error("Failed to load controls.")
        return

    for binding in DEFAULT_BINDINGS:
        controls.set_value("keybindings", binding, DEFAULT_BINDINGS[binding])
    controls.set_value("general", "version", _control_value)
    for param in OTHER_VALUES:
        controls.set_value("parameters", param, OTHER_VALUES[param])
    
    if controls.save_encrypted_pass(_CONTROLS_PATH, _PASS) != OK:
        Logging.error("Failed to save controls.")
    Logging.success("Successfully updated to default controls.")

# Loading controls settings from file.
func load_bindings() -> void:
    var controls := ConfigFile.new()
    
    if controls.load_encrypted_pass(_CONTROLS_PATH, _PASS) != OK:
        Logging.error("Failed to load controls.")
        get_tree().quit(-1)
    
    # Key bindings.
    var new_controls := {}
    for key in controls.get_section_keys("keybindings"):
        new_controls[key] = controls.get_value("keybindings", key)
    
    # Parameters (like mouse sensitivity)
    var new_params := {}
    for param in controls.get_section_keys("parameters"):
        new_params[param] = controls.get_value("parameters", param)
    
    # Remapping InputMap
    
    for action in new_controls:
        InputMap.action_erase_events(action)
        var keys : Array = new_controls[action]
        for i in range(keys.size()):
            if keys[i] != KEY_UNKNOWN:
                var key_event := InputEventKey.new()
                key_event.scancode = keys[i]
                InputMap.action_add_event(action, key_event)
                keys[i] = key_event
            else:
                keys[i] = null
        new_controls[action] = keys
    
    # Mouse sensitivity
    Player.MOUSE_SENSITIVITY = deg2rad(new_params["mouse_sensitivity"])
    print(Player.MOUSE_SENSITIVITY)
    
    _set_controls_menu_layout(new_controls, new_params)

# Setting layout of labels and button in Controls tab in Settings menu.
func _set_controls_menu_layout(new_controls: Dictionary, new_params: Dictionary) -> void:
    if not ControlsMenu:
        return
    
    # clear previous layout
    var old_controls := ControlsMenu.get_children()
    for control in old_controls:
        if InputMap.has_action(control.name):
            control.queue_free()
    
    yield(get_tree().create_timer(0.01), "timeout")
    
    var hbox := HBoxContainer.new()
    hbox.set("custom_constants/separation", 50)
    
    var font := DynamicFont.new()
    font.font_data = preload("res://Assets/Fonts/Instruction.ttf")
    font.use_filter = true
    font.size = 16
    
    var label := Label.new()
    label.align = Label.ALIGN_RIGHT
    label.rect_min_size.x = 180
    label.set("custom_fonts/font", font)
    
    var button := ControlButton.new()
    button.size_flags_horizontal = Control.SIZE_EXPAND_FILL
    button.set("custom_fonts/font", font)
    button.toggle_mode = true
    
    for action in new_controls:
        var tmp_label := label.duplicate()
        tmp_label.name = action
        tmp_label.text = str(action).replace("_", " ")
        
        var button_primary := button.duplicate()
        button_primary.name = "primary"
        if new_controls[action][0] != null:
            button_primary.text = new_controls[action][0].as_text()
            button_primary.value = new_controls[action][0].scancode
        else:
            button_primary.text = "Undefined"
        
        var button_secondary := button.duplicate()
        button_secondary.name = "secondary"
        if new_controls[action][1] != null:
            button_secondary.text = new_controls[action][1].as_text()
            button_secondary.value =\
                    new_controls[action][1].scancode
        else:
            button_secondary.text = "Undefined"
        
        var tmp_hbox := hbox.duplicate()
        
        tmp_hbox.name = action
        
        tmp_hbox.add_child(tmp_label, true)
        tmp_hbox.add_child(button_primary, true)
        tmp_hbox.add_child(button_secondary, true)
        
        ControlsMenu.add_child(tmp_hbox, true)
    
    var mouse_sens : HSlider = ControlsMenu.get_node("MouseSens/MouseSensSlider")
    mouse_sens.value = new_params["mouse_sensitivity"]
    (ControlsMenu.get_node("MouseSens/Value") as Label).text =\
            "%.2f" % mouse_sens.value


# Clearing the controls.ini file.
func clear_bindings() -> void:
    var controls := ConfigFile.new()
    
    if controls.load_encrypted_pass(_CONTROLS_PATH, _PASS) != OK:
        Logging.error("Failed to load controls.")
    
    if controls.has_section("parameters"):
        controls.erase_section("parameters")
    
    if controls.has_section("keybindings"):
        controls.erase_section("keybindings")
    else:
        for section in controls.get_sections():
            for key in controls.get_section_keys(section):
                controls.erase_section_key(section, key)
    
    if controls.save_encrypted_pass(_CONTROLS_PATH, _PASS) != OK:
        Logging.error("Failed to save controls.")

# Read new bindings from Settings menu, apply them and save.
func change_keybings() -> void:
    var gui_input := ControlsMenu.get_children()
    var controls := ConfigFile.new()
    if controls.load_encrypted_pass(_CONTROLS_PATH, _PASS) != OK:
        Logging.error("Failed to load controls file.")
        return
    
    for hbox in gui_input:
        if not InputMap.has_action(hbox.name):
            continue
        
        InputMap.action_erase_events(hbox.name)
        var primary = hbox.get_child(1)
        var secondary = hbox.get_child(2)
        
        if primary.value:
            var input_event := InputEventKey.new()
            input_event.scancode = primary.value
            InputMap.action_add_event(hbox.name, input_event)
        if secondary.value:
            var input_event := InputEventKey.new()
            input_event.scancode = secondary.value
            InputMap.action_add_event(hbox.name, input_event)
        var event1 :=\
                KEY_UNKNOWN if primary.value < 0 else primary.value
        var event2 :=\
                KEY_UNKNOWN if secondary.value < 0 else secondary.value
        controls.set_value("keybindings", hbox.name, [event1, event2])
    
    # Getting and saving mouse sensitivity.
    var mouse_sens : HBoxContainer = ControlsMenu.get_node("MouseSens")
    var value := (mouse_sens.get_node("MouseSensSlider") as HSlider).value
    controls.set_value("parameters", "mouse_sensitivity", value)
    Player.MOUSE_SENSITIVITY = deg2rad(value)
    print(Player.MOUSE_SENSITIVITY)
    
    if controls.save_encrypted_pass(_CONTROLS_PATH, _PASS) != OK:
        Logging.error("Failed to save controls file.")
        return
    Logging.success("Successfully updated controls file.")


func change_mouse_sens(value: float) -> void:
    if not Player:
        return
    
    Player.MOUSE_SENSITIVITY = deg2rad(value)
    print(Player.MOUSE_SENSITIVITY)

# To be called in SettingsMenu.gd
func set_controls_menu(menu: VBoxContainer) -> void:
    ControlsMenu = menu

# To be called in Player.gd
func set_player(player: PlayerController) -> void:
    Player = player
