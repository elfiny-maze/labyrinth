extends Node


const _CONFIG_PATH := "user://config.ini"
const _SECTION_PROPERTIES := {
    "general" : {
        "game_version" : "prealpha",
       },
    "gameplay" : {
        "show_fps" : true,
        "dev_console" : true,
        },
    "graphics" : {
        "fullscreen" : true,
        "borderless" : false,
        "stretch" : false,
        "vsync" : true,
        "resolution" : Vector3(1920, 1080, 9),
        },
    "dev_console" : {
        "max_command_number" : 15,
       },
   }
const _SETTINGS_NODES := [
    ["Gameplay", "Gameplay/VBoxContainer/GridContainer"],
    ["Graphics", "Graphics/VBoxContainer/GridContainer"],
    ["DevConsole", "DevConsole/CommandNumber"],
   ]
const POSSIBLE_RESOLUTIONS = [
    Vector2(1024, 768),
    Vector2(1280, 720),
    Vector2(1280, 800),
    Vector2(1280, 1024),
    Vector2(1360, 768),
    Vector2(1366, 768),
    Vector2(1440, 900),
    Vector2(1536, 864),
    Vector2(1600, 900),
    Vector2(1920, 1080),
]


onready var SettingsMenu : Node
onready var Settings : Dictionary


func set_settings_menu(menu: Node) -> void:
    SettingsMenu = menu
    if not SettingsMenu:
        Logging.error("SettingsMenu is null.")

    for i in _SETTINGS_NODES:
        Settings[i[0]] = menu.get_node(i[1])
        if not Settings[i[0]]:
            Logging.error("%s settings is null." % i[0])


func load_from_config_file() -> void:
    var config := ConfigFile.new()

    if config.load(_CONFIG_PATH) != OK:
        if not create_config_file():
            Logging.error(
                "Failed to load from %s/%s" %
                [OS.get_user_data_dir().replace("\\", "/"), "config.ini"]
                )
    
    var value = config.get_value("graphics", "fullscreen", _SECTION_PROPERTIES["graphics"]["fullscreen"])
    if value:
        OS.window_fullscreen = true
        OS.window_borderless = false
    else:
        OS.window_fullscreen = false
        value = config.get_value("graphics", "borderless", _SECTION_PROPERTIES["graphics"]["borderless"])
        OS.set_borderless_window(value)
        OS.window_maximized = true

    OS.set_window_position(Vector2(0, 0))
    value = config.get_value("graphics", "vsync", _SECTION_PROPERTIES["graphics"]["vsync"])
    OS.vsync_enabled = value

    value = config.get_value("graphics", "resolution", _SECTION_PROPERTIES["graphics"]["resolution"])
    yield(get_tree().create_timer(.001), "timeout")

    var stretch = config.get_value("graphics", "stretch", _SECTION_PROPERTIES["graphics"]["stretch"])
    if not stretch:
        OS.set_window_maximized(false)
        if not OS.window_fullscreen:
            OS.set_window_size(Vector2(value.x, value.y))

    ($"/root/" as Viewport).set_size(Vector2(value.x, value.y))

    # DevConole.
    value = config.get_value("gameplay", "dev_console")
    Logging.toggle_devconsole(value)
    
    if SettingsMenu:
        set_settings_menu_layout()


func set_settings_menu_layout() -> void:
    if not SettingsMenu:
        Logging.error("SettingsMenu isn't connected.")
        return
    
    var config := ConfigFile.new()
    
    if config.load(_CONFIG_PATH) != OK:
        if not create_config_file():
            Logging.error(
                "Failed to load from %s/%s" %
                [OS.get_user_data_dir().replace("\\", "/"), "config.ini"]
                )

    # Fullscreen.
    var value = config.get_value("graphics", "fullscreen")
    if value:
        Settings['Graphics'].get_node("FullScreenOption").pressed = true
        Settings['Graphics'].get_node("BorderlessOption").pressed = false
    else:
        value = config.get_value("graphics", "borderless")
        Settings['Graphics'].get_node("BorderlessOption").pressed = value

    value = config.get_value("graphics", "vsync")
    Settings['Graphics'].get_node("VSync").pressed = value

    value = config.get_value("graphics", "resolution")

    var stretch = config.get_value("graphics", "stretch")
    Settings['Graphics'].get_node("StretchOption").pressed = stretch

    Settings['Graphics'].get_node("ScreenRes/OptionButton").select(value.z)

    # FPS.
    value = config.get_value("gameplay", "show_fps")
    $"/root/Main/GUI/FPS".visible = value
    
    # DevConole.
    value = config.get_value("gameplay", "dev_console")
    Settings['Gameplay'].get_node("DevConsoleEnable").pressed = value
    SettingsMenu.set_tab_disabled(3, not value)


# Checking the validity of existing config or creating a new one
func create_config_file() -> bool:
    var file := File.new()
    var config := ConfigFile.new()

    # If file already exists...
    if file.file_exists(_CONFIG_PATH):
        # ...but we can't load it, then delete.
        if config.load(_CONFIG_PATH) != OK:
            Logging.error("Config file was probably corrupted.")
            clear_config_file()
        # ...but we can load it, then check the version.
        else:
            # If version doesn't match, delete and rewrite.
            if config.get_value("general", "version") !=\
                    _SECTION_PROPERTIES.hash():
                clear_config_file()
            # If version matches, then config file is valid.
            else:
                return true

    # If we can't create an empty file or open it as an actual file
    # then we can't do anything.
    if file.open(_CONFIG_PATH, File.WRITE) != OK:
        Logging.error("Failed to create config file.")
        file.close()
        return false
    file.close()

    # We set current version.
    config.set_value("general", "version", _SECTION_PROPERTIES.hash())
    for section in _SECTION_PROPERTIES:
        for property in _SECTION_PROPERTIES[section]:
            if not config.has_section_key(section, property):
                config.set_value(
                        section,
                        property,
                        _SECTION_PROPERTIES[section][property]
                        )
    # This is not very rational, but we rewrite this section here to get
    # current screen resolution.
    var tmp := OS.get_screen_size()
    var optButton = Settings['Graphics'].get_node("ScreenRes/OptionButton")
    config.set_value(
        "graphics",
        "resolution",
        Vector3(tmp.x, tmp.y, optButton.get_item_count() - 1)
        )
    if config.save(_CONFIG_PATH) != OK:
        Logging.error("Failed to save config file.")
    return true

# saving settings to config file
func save_to_config_file() -> void:
    if not SettingsMenu:
        Logging.error("SettingsMenu not connected.")
        return
    
    var config := ConfigFile.new()

    if config.load(_CONFIG_PATH) != OK:
        Logging.error("Failed to load the config file.")
        return

    var tmp = Settings['Gameplay'].get_node("ShowFPS").pressed
    config.set_value("gameplay", "show_fps", tmp)

    tmp = Settings['Gameplay'].get_node("DevConsoleEnable").pressed
    config.set_value("gameplay", "dev_console", tmp)

    tmp = Settings['Graphics'].get_node("FullScreenOption").pressed
    config.set_value("graphics", "fullscreen", tmp)

    tmp = Settings['Graphics'].get_node("BorderlessOption").pressed
    config.set_value("graphics", "borderless", tmp)

    tmp = Settings['Graphics'].get_node("StretchOption").pressed
    config.set_value("graphics", "stretch", tmp)

    tmp = Settings['Graphics'].get_node("VSync").pressed
    config.set_value("graphics", "vsync", tmp)

    tmp = Settings['Graphics'].get_node("ScreenRes/OptionButton")
    var index = tmp.get_selected_id()
    tmp = tmp.get_item_text(index).split('x', false)
    config.set_value("graphics", "resolution",\
            Vector3(float(tmp[0]), float(tmp[1]), index))

    tmp = Settings['DevConsole'].get_node("MaxCommandValue").value
    config.set_value("dev_console", "max_command_number", int(tmp))

    if config.save(_CONFIG_PATH) != OK:
        Logging.debug_error("Failed to update the config file.")
    else:
        Logging.debug_success(
            "Config file %s/%s has been successfully updated." %\
            [OS.get_user_data_dir().replace("\\", "/"), "config.ini"]
            )

# removing config file
func clear_config_file() -> void:
    var config := ConfigFile.new()
    
    if config.load(_CONFIG_PATH) != OK:
        Logging.error("Failed to load the config file.")
        return

    for section in config.get_sections():
        if not section in _SECTION_PROPERTIES:
            config.erase_section(section)
        else:
            for key in config.get_section_keys(section):
                if not key in _SECTION_PROPERTIES[section]:
                    config.erase_section_key(section, key)

    if config.save(_CONFIG_PATH) != OK:
        Logging.error("Failed to save the config file.")
