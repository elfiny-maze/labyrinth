extends Control


export var TOOLTIP_DELAY := .75
export var STATIC := true
const TOOLTIP_OFFSET := Vector2(6,6)


onready var TOOLTIP := preload("res://Main/CustomTooltip.tscn")
onready var cur_tooltip : HBoxContainer = null


func _process(_delta: float) -> void:
    if cur_tooltip:
        if visible:
            cur_tooltip.rect_position = get_global_mouse_position() +\
                    TOOLTIP_OFFSET
            if not STATIC:
                cur_tooltip.set_text(get_tooltip())
        else:
            cur_tooltip.visible = false
            cur_tooltip.queue_free()

func _notification(what: int) -> void:
    match (what):
        NOTIFICATION_MOUSE_ENTER, NOTIFICATION_FOCUS_ENTER:
            if not cur_tooltip:
                cur_tooltip = TOOLTIP.instance()
                yield(get_tree().create_timer(TOOLTIP_DELAY), "timeout")
                if cur_tooltip:
                    cur_tooltip.rect_position = get_global_mouse_position() +\
                            TOOLTIP_OFFSET
                    cur_tooltip.set_text(get_tooltip())
                    $"/root/Main".add_child(cur_tooltip)
        NOTIFICATION_MOUSE_EXIT,\
        NOTIFICATION_FOCUS_EXIT,\
        NOTIFICATION_VISIBILITY_CHANGED:
            if cur_tooltip:
                cur_tooltip.visible = false
                cur_tooltip.queue_free()
