extends Node

# Dublicate of one in DevConsole.
enum CATEGORY {NORMAL, WARNING, ERROR, SUCCESS, PRETTY}

signal process_message(message, CATEGORY)
signal toggle_devconsole(value)

# Normal logging.
func message(message) -> void:
    emit_signal(
            "process_message",
            message if typeof(message) == TYPE_STRING else str(message),
            CATEGORY.NORMAL
            )

# Debug-only normal logging.
func debug_message(message) -> void:
    if OS.is_debug_build():
        emit_signal(
                "process_message",
                message if typeof(message) == TYPE_STRING else str(message),
                CATEGORY.NORMAL
                )

# Error logging.
func error(message) -> void:
    emit_signal(
            "process_message",
            message if typeof(message) == TYPE_STRING else str(message),
            CATEGORY.ERROR
            )


func debug_error(message) -> void:
    if OS.is_debug_build():
        emit_signal(
                "process_message",
                message if typeof(message) == TYPE_STRING else str(message),
                CATEGORY.ERROR
                )

# Warning logging.
func warning(message) -> void:
    emit_signal(
            "process_message",
            message if typeof(message) == TYPE_STRING else str(message),
            CATEGORY.WARNING
            )


func debug_warning(message) -> void:
    if OS.is_debug_build():
        emit_signal(
                "process_message",
                message if typeof(message) == TYPE_STRING else str(message),
                CATEGORY.WARNING
                )

# Success logging.
func success(message) -> void:
    emit_signal(
            "process_message",
            message if typeof(message) == TYPE_STRING else str(message),
            CATEGORY.SUCCESS
            )


func debug_success(message) -> void:
    if OS.is_debug_build():
        emit_signal(
                "process_message",
                message if typeof(message) == TYPE_STRING else str(message),
                CATEGORY.SUCCESS
                )

# Pretty color logging.
func special(message) -> void:
    emit_signal(
            "process_message",
            message if typeof(message) == TYPE_STRING else str(message),
            CATEGORY.SPECIAL
            )


func debug_special(message) -> void:
    if OS.is_debug_build():
        emit_signal(
                "process_message",
                message if typeof(message) == TYPE_STRING else str(message),
                CATEGORY.SPECIAL
                )


func toggle_devconsole(value: bool) -> void:
    emit_signal("toggle_devconsole", value)
