extends Button
class_name ControlButton


var value : int = -1
var waiting_input := false


func _ready() -> void:
    set("custom_colors/font_color", Color.white)


func _input(event: InputEvent) -> void:
    if waiting_input:
        if event is InputEventKey and not event.pressed:
            if event.scancode != KEY_ESCAPE:
                value = event.get_scancode_with_modifiers()
                text = OS.get_scancode_string(value)
                pressed = false
                waiting_input = false
        if event is InputEventMouseButton or\
            (event is InputEventKey and event.scancode == KEY_ESCAPE):
            if value >= 0:
                text = OS.get_scancode_string(value)
            else:
                text = "Undefined"
            pressed = false
            waiting_input = false
        look_up_conflicts()


func _toggled(button_pressed: bool) -> void:
    if button_pressed:
        waiting_input = true
        set_text("Press Any Key")


func look_up_conflicts() -> void:
    if not self.value:
        self.set("custom_colors/font_color", Color.white)
        return
    
    var has_conflicts := false
    
    var input_actions : Array = Controls.ControlsMenu.get_children()
    for hbox in input_actions:
        if not InputMap.has_action(hbox.name):
            continue
        var buttons := [hbox.get_child(1), hbox.get_child(2)]
        
        for button in buttons:
            if self != button:
                if button.value >= 0 and value == button.value:
                    set("custom_colors/font_color", Color.red)
                    button.set("custom_colors/font_color", Color.red)
                    has_conflicts = true
                else:
                    button.set("custom_colors/font_color", Color.white)
    if not has_conflicts:
        set("custom_colors/font_color", Color.white)
