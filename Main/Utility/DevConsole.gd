extends VBoxContainer


var MAX_COMMANDS : int = 15
var COMMAND_LIST := {
    "clear" : [
        funcref(self, "clear"),
        "Clears all console output content.",
        ],
    "help" : [
        funcref(self, "help"),
        "Displays all available commands.",
        ],
    "echo" : [
        funcref(self, "echo"),
        "Prints a statement",
        ],
    "fly" : [
        funcref(self, "fly"),
        "Toggles player's ability to fly",
       ],
    "teleport" : [
        funcref(self, "teleport"),
        "Changes level",
       ],
    "maps" : [
        funcref(self, "maps"),
        "Lists all available maps.",
       ],
    "resolution" : [
        funcref(self, "resolution"),
        "Changes screen resolution",
       ],
    "notooltips" : [
        funcref(self, "notooltips"),
        "Removes bugged tooltips off screen.",
       ],
    "exit" : [
        funcref(self, "exit"),
        "Quits the console",
        ],
   }


const CONSOLE_COLORS := {
    Logging.CATEGORY.NORMAL : 0xffffffff, # White.
    Logging.CATEGORY.WARNING : 0xffbb1aff, # Orange.
    Logging.CATEGORY.ERROR : 0xdc341eff, # Red.
    Logging.CATEGORY.SUCCESS : 0x36f925ff, # Green.
    Logging.CATEGORY.PRETTY : 0x10ffbcff, # Purple.
   }

onready var ConsoleFont := DynamicFont.new()
onready var Scroller : ScrollContainer = $CenterContainer/ScrollContainer
onready var Commands : VBoxContainer =\
        $CenterContainer/ScrollContainer/HBoxContainer
onready var VisibleCommands :=\
        [$CenterContainer/ScrollContainer/HBoxContainer/Label]
onready var Player : PlayerController = $"/root/Main/Player"
onready var enabled := true


func _ready() -> void:
    ($CenterContainer/Background as ColorRect).rect_min_size = Vector2(0, 200)
    ($CenterContainer/ScrollContainer as ScrollContainer).follow_focus = true
    VisibleCommands[0].set(
                "custom_colors/font_color",
                CONSOLE_COLORS[Logging.CATEGORY.PRETTY]
                )

    if Logging.connect("process_message",\
            $".", "print_dev") != OK:
        print("Failed to connect Logging message processing.")
        get_tree().quit(-1)
    
    if Logging.connect("toggle_devconsole", $".", "toggle_enabled") != OK:
        print("Failed to connect DevConsole to Logging.")
        get_tree().quit(-1)

    ConsoleFont = preload("res://Assets/Fonts/DevConsole.res")


func _input(event: InputEvent) -> void:
    if event.is_action_pressed("dev_console") and enabled:
        visible = not visible
    elif event.is_action_pressed("pause"):
        ($InputLine as LineEdit).release_focus()

# Entering command.
func _on_InputLine_text_entered(new_text: String) -> void:
    ($InputLine as LineEdit).text = ""
    var command : Array = new_text.split(" ", false)
    if command[0] in COMMAND_LIST:
        COMMAND_LIST[command.pop_front()][0].call_func(command)
        
    else:
        add_line("Unknown command.", Logging.CATEGORY.ERROR)


func toggle_enabled(is_enabled: bool) -> void:
    enabled = is_enabled
    if visible and not enabled:
        visible = false


# in-game print()
func print_dev(message: String, cat := Logging.CATEGORY.NORMAL) -> void:
    var cur_date := OS.get_datetime()
    var msg = "[%02d:%02d:%02d] %s" % [
        cur_date.hour, cur_date.minute, cur_date.second, 
        message,
    ]
    print(msg)
    add_line(msg, cat)

# Adding new line.
func add_line(new_line: String, cat := Logging.CATEGORY.NORMAL) -> void:
    var new_label := Label.new()
    new_label.text = new_line
    new_label.set("custom_fonts/font", ConsoleFont)
    new_label.set("custom_colors/font_color", Color(CONSOLE_COLORS[cat]))
    
    VisibleCommands.push_back(new_label)
    if VisibleCommands.size() > MAX_COMMANDS:
        VisibleCommands.pop_front().queue_free()
    Commands.add_child(new_label)
    yield(get_tree(), "idle_frame")
    if Scroller.get_v_scrollbar().visible:
        Scroller.scroll_vertical = int(Scroller.get_v_scrollbar().max_value)

# Just clear all console.
func clear(args: PoolStringArray) -> void:
    if not args.empty():
        add_line("Invalid arguments.", Logging.CATEGORY.ERROR)
    else:
        for command in VisibleCommands:
            command.queue_free()
        VisibleCommands.clear()

# Print all commands with meanings.
func help(args: PoolStringArray) -> void:
    if not args.empty():
        add_line("Invalid arguments.", Logging.CATEGORY.ERROR)
    else:
        add_line("Available commands:", Logging.CATEGORY.PRETTY)
        for command in COMMAND_LIST:
            add_line("%-15s%s" % [
                command,
                COMMAND_LIST[command][1]
                ])

# Just simple print.
func echo(args: PoolStringArray) -> void:
    add_line(args.join(" "))

# Toggles fly.
func fly(args: PoolStringArray) -> void:
    if args.size() == 1:
        if args[0] == "ON":
            Player.is_flying = true
            add_line("Fly turned on.")
        elif args[0] == "OFF":
            Player.is_flying = false
            add_line("Fly turned off.")
        else:
            add_line("Invalid argument.", Logging.CATEGORY.ERROR)
            return
    elif args.empty():
        Player.toggle_fly()
        add_line("Fly toggled.")
    else:
        add_line("Invalid argument.", Logging.CATEGORY.ERROR)

# Changes current level
func teleport(args: PoolStringArray) -> void:
    if args.size() != 1:
        add_line("Invalid arguments.", Logging.CATEGORY.ERROR)
    else:
        var Main := $"/root/Main"
        var current_level = $"/root/Main/Level"
        Main.remove_child(current_level)
        if not Main.level_translation(args[0]):
            add_line("Invalid level name.", Logging.CATEGORY.ERROR)
            Main.add_child(current_level)
        else:
            add_line("Teleported to %s" % args[0])


func maps(args: PoolStringArray) -> void:
    if not args.empty():
        add_line("Invalid arguments.", Logging.CATEGORY.ERROR)
    else:
        for map in ($"/root/Main".ALL_LEVELS as Dictionary):
            add_line(map as String)


func resolution(args: PoolStringArray) -> void:
    var root : Viewport = $"/root/"
    if args.empty():
        var res := root.size
        add_line("%dx%d" % [res.x, res.y])
    elif args.size() != 2:
        add_line("Invalid arguments.", Logging.CATEGORY.ERROR)
    else:
        var res := Vector2(float(args[0]), float(args[1]))
        if res.length_squared() > OS.get_screen_size().length_squared():
            add_line("Too big value.", Logging.CATEGORY.ERROR)
        else:
            root.size = res
            add_line("Screen resolution has been updated.",
                    Logging.CATEGORY.SUCCESS)


func notooltips(args: PoolStringArray) -> void:
    if not args.empty():
        add_line("Invalid arguments.", Logging.CATEGORY.ERROR)
    else:
        get_tree().call_group("tooltips", "free")


# Clear and close DevConsole.
func exit(args: PoolStringArray) -> void:
    if not args.empty():
        add_line("Invalid arguments.", Logging.CATEGORY.ERROR)
    else:
        clear([])
        add_line("Welcome to DevConsole! ;) type help", Logging.CATEGORY.PRETTY)
        visible = false
