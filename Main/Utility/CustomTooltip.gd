extends HBoxContainer


onready var label : Label = $Label


func set_text(new_text: String) -> void:
    label.text = new_text
