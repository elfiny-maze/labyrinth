extends Control


onready var SettingsMenu := $SettingsPanel


func _ready() -> void:
    pause_mode = Node.PAUSE_MODE_PROCESS

    var hbox : VBoxContainer = $Background/HBoxContainer
    hbox.margin_right = max(($"/root/" as Viewport).size.x / 12, 100)
    hbox.margin_left = -hbox.margin_right

    var settings_cont : VBoxContainer = $SettingsPanel
    settings_cont.margin_left = min(($"/root/" as Viewport).size.x / 6, 100)
    settings_cont.margin_right = -settings_cont.margin_left
    settings_cont.margin_top = min($"/root/".size.y / 12, 10)
    settings_cont.margin_bottom = -settings_cont.margin_top
    
    ($SettingsPanel/CenterContainer as Control).margin_bottom += 20
    ($SettingsPanel/CenterContainer as Control).margin_top -= 20


func _input(event: InputEvent) -> void:
    if event.is_action_pressed("pause"):
        if SettingsMenu.visible:
            SettingsMenu.visible = false
        else:
            if visible:
                Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
            else:
                Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
            custom_pause(visible, visible)
            visible = not visible


func custom_pause(process_state: bool, input_state: bool) -> void:
    get_tree().call_group("pausable", "set_process", process_state)
    get_tree().call_group("pausable", "set_physics_process", process_state)
    get_tree().call_group("pausable", "set_process_input", input_state)
    get_tree().call_group("pausable",
            "set_process_unhandled_input",
            input_state
            )
    get_tree().call_group("pausable",
            "set_process_unhandled_key_input",
            input_state
            )

# To continue the game.
func _on_ResumeButton_pressed() -> void:
    Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
    custom_pause(true, true)
    visible = false


# Closing the game.
func _on_ExitButton_pressed() -> void:
    Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
    # TODO: temporarily...
    var packed_scene := load("Main/MainMenu.tscn")
    $"/root/".add_child(packed_scene.instance())
    $"/root/Main".queue_free()


# Reload the scene.
func _on_RestartButton_pressed() -> void:
    custom_pause(true, true)
    if get_tree().reload_current_scene() != OK:
        get_tree().quit(-1)


# Open settings menu.
func _on_SettingsButton_pressed() -> void:
    SettingsMenu.visible = true
