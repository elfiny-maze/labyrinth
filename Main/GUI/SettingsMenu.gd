extends TabContainer


onready var GUI := $"/root/Main/GUI"
onready var ScreenOptions :=\
         $Graphics/VBoxContainer/GridContainer/ScreenRes/OptionButton
onready var CommandSlider := $DevConsole/CommandNumber/MaxCommandValue

onready var _controls_buttons : Control = $"../CenterContainer/HBoxControls"
onready var _general_buttons : Control = $"../CenterContainer/HBoxGeneral"


func _ready() -> void:
    CommandSlider.hint_tooltip = str(CommandSlider.value)
    
    Config.set_settings_menu(self)
    Controls.set_controls_menu($Controls/ScrollContainer/ControlKeys as VBoxContainer)
    
    if not Config.create_config_file():
        Logging.error("Failed to create a config file.")
    
    prepare_screen_resolution()
    Config.set_settings_menu_layout()
    Controls.load_bindings()
    
    var screen := ($"/root/" as Viewport).size
    
    var ControlKeys : Control = $Controls/ScrollContainer
    ControlKeys.margin_left = screen.x / 15
    ControlKeys.margin_right = -screen.x / 15
    ControlKeys.margin_bottom = -50
    ControlKeys.margin_top = 20
    
    var controls_buttons :=\
            $"../CenterContainer/HBoxControls".get_children()
    if (controls_buttons[0] as Button).connect(
            "pressed",
            Controls,
            "change_keybings") != OK:
                Logging.error("Failed to connect Save Button to Controls.")
    if (controls_buttons[1] as Button).connect(
            "pressed",
            Controls,
            "load_bindings") != OK:
                Logging.error("Failed to connect Discard Button to Controls.")
    if (controls_buttons[2] as Button).connect(
            "pressed",
            Controls,
            "set_default") != OK:
                Logging.error("Failed to connect Default Button to Controls.")



func prepare_screen_resolution() -> void:
    var screen_menu : OptionButton =\
            $Graphics/VBoxContainer/GridContainer/ScreenRes/OptionButton
    var max_size := OS.get_screen_size().length()
    for res in Config.POSSIBLE_RESOLUTIONS:
        if res.length() <= max_size:
            screen_menu.add_item("%dx%d" % [res.x, res.y])
    screen_menu.select(screen_menu.get_item_count() - 1)


func _on_MaxCommandValue_value_changed(value: float) -> void:
    CommandSlider.hint_tooltip = str(value)


# Discarding settings to previous configuration
func _on_Discard_pressed():
    Config.load_from_config_file()

# Applying settings
func _on_Apply_pressed():
    Config.save_to_config_file()
    Config.load_from_config_file()


func _on_SettingsMenu_tab_changed(tab: int) -> void:
    if tab == 1:
        _controls_buttons.visible = true
        _general_buttons.visible = false
    else:
        _controls_buttons.visible = false
        _general_buttons.visible = true


func _on_OpenUserDir_pressed():
    if OS.shell_open(OS.get_user_data_dir()) != OK:
        Logging.error("Failed to open user data directory.")
