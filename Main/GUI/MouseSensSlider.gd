extends HSlider


onready var _value_displayer : Label = $"../Value"


func _ready() -> void:
    value = Controls.OTHER_VALUES["mouse_sensitivity"]
    _on_MouseSensSlider_value_changed(value)


func _on_MouseSensSlider_value_changed(value: float) -> void:
    _value_displayer.text = "%.2f" % value
