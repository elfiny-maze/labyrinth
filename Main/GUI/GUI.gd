extends Control


onready var SettingsMenu : TabContainer = $PauseMenu/SettingsPanel/SettingsMenu
onready var InventoryPopup : Inventory = $Inventory


func _ready() -> void:
    # Preparing GUI.
    ($PauseMenu/SettingsPanel as Control).visible = false
    ($PauseMenu as Control).visible = false

func _input(event: InputEvent) -> void:
    if event.is_action_pressed("inventory"):
        if InventoryPopup.is_processing() and not InventoryPopup.visible:
            InventoryPopup.popup()
        else:
            InventoryPopup.hide()
