extends KinematicBody


##############################
# ENEMY'S BEHAVIOR
#
# 1. Not seeing the player:
#    randonly moves around on short distances,
#    stays on one place for a short period of time,
#    rotates in random direction, but avoids walls
#
# 2. Upon seeing the player:
#    looks at her and approaches with malicious intent
#
# 3. Upon losing the player
#    Moves slower in the same direction, and then, if
#    no player was found, returns to (1).
#
##############################

const ROTATE_ACCEL := 8.0
const IDLE_ROTATE_SPEED := 0.1

onready var health := 10.0
onready var attack := 5.0
onready var run_speed := 5.0
onready var velocity := Vector3.ZERO
onready var Player: Node = null


func _ready() -> void:
    add_to_group("pausable")


func _process(delta) -> void:
    var begin = transform.origin
    var end = $"/root/Main/Player".transform.origin
    var p = $"/root/Main".get_simple_path(begin, end, true)
    var path := Array(p) # Vector3 array too complex to use, convert to regular array.
    path.invert()
    set_process(true)

    var im = $"/root/Main/Draw"
    im.clear()
    im.begin(Mesh.PRIMITIVE_POINTS, null)
    im.add_vertex(begin)
    im.add_vertex(end)
    im.end()
    im.begin(Mesh.PRIMITIVE_LINE_STRIP, null)
    for x in p:
        im.add_vertex(x)
    im.end()


func _physics_process(delta: float) -> void:
    var target: Basis
    if Player:
        target = follow(delta)
    else:
        target = idle(delta)
    target = target.orthonormalized()
    transform.basis = transform.basis.slerp(target, delta * ROTATE_ACCEL)


func _on_DetectionArea_body_entered(body: PlayerController) -> void:
    if body is PlayerController:
        Logging.warning("[Enemy Cube] I saw someone!")
        Player = body


func _on_DetectionArea_body_exited(body: PlayerController) -> void:
    if body is PlayerController:
        Logging.warning("[Enemy Cube] I don't see them anymore.")
        Player = null


func follow(_delta: float) -> Basis:
    return transform.looking_at(Player.transform.origin, Vector3.UP).basis


func idle(_delta: float) -> Basis:
    return Basis(Vector3(0.0, rotation.y + IDLE_ROTATE_SPEED, 0))
